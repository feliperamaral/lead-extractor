<?php

namespace AlpesPlugin\LeadExtractor\Classes\Parser;

use AlpesPlugin\LeadExtractor\Classes\Parser\Adapter\AdapterInterface as ParseAdapter;
use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;

class Parser
{
    /* @var $variable AdapterInterface */

    protected $adapter;

    public function __construct(ParseAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function parse(AbstractSourceLoader $source)
    {
        $output = $source->getOutput();
        $parsed = $this->adapter->parse($output);

        return $parsed;
    }
}
