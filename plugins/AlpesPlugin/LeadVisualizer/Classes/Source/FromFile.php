<?php

namespace AlpesPlugin\LeadExtractor\Classes\Source;

/**
 * Recebe um arquivo por upload que será enviado ao parser
 */
class FromFile extends AbstractSourceLoader
{

    public function getOutput()
    {
        $file = $_FILES[self::class]['tmp_name']['input'];

        return file_get_contents($file);
    }
}
