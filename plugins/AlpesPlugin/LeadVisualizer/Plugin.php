<?php

namespace AlpesPlugin\LeadVisualizer;

use BackendMenu;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {

        return [
            'name' => 'Lead Visualizer',
            'description' => 'Visualiza de forma amigável os leads',
            'author' => 'Felipe Amaral',
            'icon' => 'icon-paste'
        ];
    }

    public function boot()
    {
        /**
         * O autoload está sendo configurado aqui pois o "composer install" recursivo
         * não está lendo esse diretório devido ao build do Docker
         */
        /* @var $autoload \Composer\Autoload\ClassLoader */
        $autoload = require 'vendor/autoload.php';
        $autoload->addPsr4('AlpesPlugin\LeadVisualizer\\', __DIR__);

        $this->registerSideMenuItem();
    }

    public function registerSideMenuItem()
    {

        $sideMenu = $this->getConfigurationFromYaml()['side-menu'];

        BackendMenu::registerCallback(function ($manager) use($sideMenu) {

            extract($sideMenu);
            if (isset($definition['url'])) {
                $urlBase = \Config::get('cms.backendUri', 'backend') . '/alpes-panel';
                $definition['url'] = '/' . $urlBase . '/' . ltrim($definition['url'], '\/');
            }
            $manager->addSideMenuItem($owner, $code, $sideCode, $definition);
        });
    }
}
