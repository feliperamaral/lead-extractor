<?php

namespace AlpesPlugin\LeadVisualizer\Controllers;

use AlpesPlugin\LeadExtractor\Classes\Parser\Adapter\AdapterInterface as AdapterParserInterface;
use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;
use Backend\Classes\Controller;
use Db;
use October\Rain\Support\Facades\Flash;
use Cms\Widgets\ComponentList;

class Index extends Controller
{

    protected $leadsData;

    public function __construct()
    {
        parent::__construct();

        new ComponentList($this, 'componentList');

        $this->addCss('/modules/cms/assets/css/october.components.css', 'core');
    }

    public function index()
    {
        $this->pageTitle = 'Lead Visualizer';
        /* @var $leadsTable \October\Rain\Database\QueryBuilder */
        $leadsTable = Db::table('alpes_leads');
        $this->leadsData = $leadsTable->paginate();
        
        $this->vars['recordTotal'] = $this->leadsData->total();
        $this->vars['pageCurrent'] = $this->leadsData->currentPage();
        $this->vars['pageLast'] = $this->leadsData->lastPage();
        $this->vars['pageFrom'] = $this->leadsData->firstItem();
        $this->vars['pageTo'] = $this->leadsData->lastItem();
    }
}
