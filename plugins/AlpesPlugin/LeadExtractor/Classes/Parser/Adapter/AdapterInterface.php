<?php

namespace AlpesPlugin\LeadExtractor\Classes\Parser\Adapter;

use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;

interface AdapterInterface
{

    public function parse(AbstractSourceLoader $source);
}
