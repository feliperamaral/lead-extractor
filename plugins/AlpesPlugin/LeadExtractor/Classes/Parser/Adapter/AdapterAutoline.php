<?php

namespace AlpesPlugin\LeadExtractor\Classes\Parser\Adapter;

use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;

class AdapterAutoline implements AdapterInterface
{

    public function __construct()
    {
        throw new \Error('Adapter não implementado');
    }

    public function parse(AbstractSourceLoader $source)
    {
        $output = $source->getOutput();

        // Processa o $output de acordo com as necessidades da Autoline

        return $output;
    }
}
