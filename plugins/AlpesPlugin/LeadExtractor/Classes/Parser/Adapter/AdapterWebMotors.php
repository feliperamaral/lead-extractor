<?php

namespace AlpesPlugin\LeadExtractor\Classes\Parser\Adapter;

use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;
use Symfony\Component\DomCrawler\Crawler;

class AdapterWebMotors implements AdapterInterface
{

    protected $trCallbacks = [
        0 => 'leadDetails',
        1 => 'seller',
        2 => 'vehicle',
        3 => 'date'
    ];

    public function parse(AbstractSourceLoader $source)
    {
        $output = $source->getOutput();

        $crawler = new Crawler($output);
        $tableTr = $crawler->filter('.gestao-leads #container-lista .tabela table tbody tr');

        $self = $this;
        $data = [];
        $tableTr->each(function(Crawler $tr) use($self, &$data) {
            $dataTr = [];
            $tr->filter('td')->each(function(Crawler $td, $idx)use($self, &$dataTr) {
                $res = [$self, $self->trCallbacks[$idx]]($td);
                $dataTr = ($dataTr + (array) $res);
            });
            $data[] = $dataTr;
        });

        return $data;
    }

    protected function leadDetails(Crawler $doc)
    {
        $details = [];
        $doc->filter('td > div')->each(function(Crawler $doc, $idx) use(&$details) {
            switch ($idx) {
                case 0:
                    $details['indice_0'] = trim($doc->text());
                    break;
                case 1:
                    list($type, $text) = explode(':', $doc->text(), 2);
                    $details['type'] = trim($type);
                    $details['details'] = trim($text);
                    break;
                case 2:
                    $details['status'] = trim($doc->text());
                    $details['free_lead'] = stripos($details['status'], 'gratuito') !== false;
                    $details['status'] = trim(str_ireplace('gratuito', '', $details['status']));
                    break;
                default:
                    break;
            }
        });
        switch (strtolower($details['type'])) {
            case 'proposta':
                $details['client_name'] = $details['indice_0'];
                $details['phone'] = null;
                break;
            case 'ligação':
                $details['client_name'] = null;
                $details['phone'] = $details['indice_0'];
                break;
            default:
                break;
        }
        unset($details['indice_0']);
        return $details;
    }

    protected function seller(Crawler $doc)
    {
        return ['seller' => trim($doc->text())];
    }

    protected function vehicle(Crawler $doc)
    {
        if (preg_match('/Ve(í|i)culo\s*n(ã|a)o\s*informado/i', $doc->text()) > 0) {
            return ['vehicle' => null];
        }
        $vehicle = [];
        $doc->filter('td > div')->each(function(Crawler $doc, $idx) use (&$vehicle) {
            switch ($idx) {
                case 0:
                    preg_match('/'
                        . '(?P<marca>.*?\s{2,})'
                        . '(?P<modelo>.*)'
                        . '(?P<ano>[1-2][0-9]{3}\\/[1-2][0-9]{3})'
                        . '/', trim($doc->text()), $matches);
                    $vehicle['marca'] = $matches['marca'];
                    $vehicle['modelo'] = $matches['modelo'];
                    $vehicle['ano'] = $matches['ano'];
                    break;
                case 1:
                    $vehicle['desc'] = $doc->text();
                    break;
                case 2:
                    $vehicle['estado'] = $doc->filter('span:first-child')->eq(0)->text();
                    $vehicle['placa'] = $doc->filter('span:last-child')->eq(0)->text();
                    break;
                default:
                    break;
            }
        });

        return [
            'vehicle' => array_map('trim', $vehicle)
        ];
    }

    protected function date(Crawler $doc)
    {
        $at = $doc->filter('div:first-child')->eq(0)->text();
        $time = $doc->filter('div:last-child')->eq(0)->text();
        list($hour, $minute) = explode(':', $time);

        $date = new \DateTime();
        $date->setTime($hour, $minute, 0);

        if (strtolower(trim($at)) == 'ontem') {
            $date->sub(date_interval_create_from_date_string('1 days'));
        }

        return [
            'date' => $date
        ];
    }
}
