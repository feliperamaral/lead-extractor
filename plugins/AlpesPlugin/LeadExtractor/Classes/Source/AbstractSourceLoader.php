<?php

namespace AlpesPlugin\LeadExtractor\Classes\Source;

abstract class AbstractSourceLoader
{

    abstract public function getOutput();
}
