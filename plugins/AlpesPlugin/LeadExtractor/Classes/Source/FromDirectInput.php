<?php

namespace AlpesPlugin\LeadExtractor\Classes\Source;

/**
 * Esse load seria capaz de ler uma entrada direta o usuário
 * ctrl+c, ctrl+v
 */
class FromDirectInput extends AbstractSouceLoader
{
    /**
     * Essa classe existe apenas em caráter ilustrativo
     *  para mostrar a extensibilidade do plugin
     *  
     */
}
