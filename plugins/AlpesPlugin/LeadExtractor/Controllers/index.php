<?php

namespace AlpesPlugin\LeadExtractor\Controllers;

use AlpesPlugin\LeadExtractor\Classes\Parser\Adapter\AdapterInterface as AdapterParserInterface;
use AlpesPlugin\LeadExtractor\Classes\Source\AbstractSourceLoader;
use Backend\Classes\Controller;
use Db;
use October\Rain\Support\Facades\Flash;
use Cms\Widgets\ComponentList;

class Index extends Controller
{

    public function __construct()
    {
        parent::__construct();

        new ComponentList($this, 'componentList');

        $this->addCss('/modules/cms/assets/css/october.components.css', 'core');
    }

    public function index()
    {
        $this->pageTitle = 'Lead Extractor';

        if ($_POST && isset($_POST['parser']) && $_POST['parser']) {
            $data = $this->process($_POST['parser'], $_POST['source_type']);
            $this->viewData = new \stdClass();
            try {
                $this->saveLeads($data);

                Flash::success('Importado ' . sizeof($data) . ' leads');
            } catch (\Exception $ex) {
                Flash::error('Erro ao extrair leads: ' . $ex->getMessage());
            }
        }
    }

    public function process(string $parserAdapterClass, string $sourceClass)
    {
        $parserAdapterClass = stripcslashes($parserAdapterClass);
        /* @var $source AdapterParserInterface */
        $parser = new $parserAdapterClass;
        if (!$parser instanceof AdapterParserInterface) {
            throw new \Exception('A classe deve "' . $parserAdapterClass . '" DEVE implementar interface "' . AdapterParserInterface::class . '"');
        }

        $sourceClass = stripcslashes($sourceClass);
        /* @var $source AbstractSourceLoader */
        $source = new $sourceClass;
        if (!$source instanceof AbstractSourceLoader) {
            throw new \Exception('A classe deve "' . $sourceClass . '" DEVE implementar interface "' . AbstractSourceLoader::class . '"');
        }
        $data = $parser->parse($source);
        return $data;
    }

    public function saveLeads($data)
    {
        /* @var $leadsTable \October\Rain\Database\QueryBuilder */
        $leadsTable = Db::table('alpes_leads');

        foreach ($data as $row) {
            if (is_array($row['vehicle'])) {
                $row['vehicle'] = json_encode($row['vehicle']);
            }

            $leadsTable->insert($row);
        }
    }
}
