FROM dragontek/octobercms:7.0-apache


COPY ./plugins/AlpesPlugin /usr/src/october/plugins/AlpesPlugin
COPY ./modules/alpespanel /usr/src/october/modules/alpespanel
COPY ./config/cms.php /usr/src/october/config/cms.php

RUN chown -R www-data.www-data /usr/src/october/plugins/AlpesPlugin \
    && chown -R www-data.www-data /usr/src/october/modules/alpespanel 