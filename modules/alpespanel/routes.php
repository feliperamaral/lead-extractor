<?php
Event::listen('backend.beforeRoute', function () {
    $backendPrefix = Config::get('cms.backendUri', 'backend') . '/alpes-panel';

    Route::group(['prefix' => $backendPrefix], function () {
        Route::any('{slug}', 'AlpesPanel\Classes\BackendController@run')
            ->where('slug', '(.*)?');
    });

    Route::any($backendPrefix, 'AlpesPanel\Classes\BackendController@run');
});
