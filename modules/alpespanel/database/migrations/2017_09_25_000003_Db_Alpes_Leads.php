<?php

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class DbAlpesLeads extends Migration
{

    public function up()
    {
        Schema::create('alpes_leads', function (Blueprint $table) {
            $table->integer('lead_id', true, true)->nullable();
            $table->string('status', 50)->nullable();
            $table->tinyInteger('free_lead')->nullable();
            $table->string('type', 50)->nullable();
            $table->string('details', 250)->nullable();
            $table->string('client_name', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->string('seller', 250)->nullable();
            $table->text('vehicle')->nullable();
            $table->dateTime('date')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alpes_leads');
    }
}
