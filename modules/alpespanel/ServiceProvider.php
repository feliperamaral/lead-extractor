<?php

namespace AlpesPanel;

use App;
use Lang;
use Event;
use Backend;
use BackendMenu;
use BackendAuth;
use Backend\Classes\WidgetManager;
use October\Rain\Support\ModuleServiceProvider;
use System\Classes\SettingsManager;
use System\Classes\CombineAssets;
use Cms\Classes\ComponentManager;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\CmsObject;
use Cms\Models\ThemeData;
use Cms\Models\ThemeLog;
use Backend\Classes\NavigationManager;

class ServiceProvider extends ModuleServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::register('alpespanel');

        /*
         * Backend specific
         */
        if (App::runningInBackend()) {
            $this->registerBackendNavigation();
        }
    }
    /*
     * Register navigation
     */

    protected function registerBackendNavigation()
    {
        BackendMenu::registerCallback(function (NavigationManager $manager) {
            /* @var $manager NavigationManager */

            $manager->registerMenuItems('Alpes.Panel', [
                'alpespanel' => [
                    'label' => 'Alpes Panel',
                    'icon' => 'icon-magic',
                    'url' => Backend::url('alpes-panel'),
                    'permissions' => [
                    ],
                    'order' => 1000,
                    'sideMenu' => [
                        'components' => [
                            'label' => 'cms::lang.component.menu_label',
                            'icon' => 'icon-puzzle-piece',
                            'url' => 'javascript:;',
                            'attributes' => ['data-menu-item' => 'components'],
                            'permissions' => ['cms.manage_pages', 'cms.manage_layouts', 'cms.manage_partials']
                        ]
                    ]
                ],
            ]);
            
        });
    }
}
